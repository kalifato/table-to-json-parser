var express = require('express');
var router = express.Router();
const cheerio = require("cheerio");
const sa = require('superagent');
var dayjs = require('dayjs');
var customParseFormat = require('dayjs/plugin/customParseFormat');
dayjs.extend(customParseFormat);

var path = require('path');
var dotenv = require('dotenv');

const envPath = path.join(__dirname, '..', '.env');
const environ = dotenv.config({path: envPath}).parsed;

/* GET users listing. */
router.get('/', async function (req, res, next) {
  const html = await getHtml();
  const objects = process(html);
  res.send(objects);
});

async function getHtml() {
  return sa
    .get(environ.GETURI)
    .then(res => res.text);
}

function process(data) {
  let $ = cheerio.load(data);

  let c = ['id', 'timestamp', 'longitude', 'latitude', 'battery', 'gpsSignal', 'rssi', 'snr', 'temperature', 'upl'];

  let rawData = $('table tr td').toArray().map(item => {
    let d = $(item).text().trim();
    d = isNaN(d) ? d : +d;
    const dateFormat = 'YYYY-MM-DD HH:mm:ss';
    const isDate = dayjs(d, dateFormat).isValid();
    d = isDate ? dayjs(d, dateFormat) : d;
    return d;
  });

  rawData = [...c, ...rawData];

  let result = arrayTo2DArray2(rawData, 10);

  const [keys, ...values] = result;
  const objects = values.map(array => array.reduce((a, v, i) => ({...a, [keys[i]]: v}), {}));

  return objects;
}

function arrayTo2DArray2(list, howMany) {
  var idx = 0;
  result = [];

  while (idx < list.length) {
    if (idx % howMany === 0) result.push([]);
    result[result.length - 1].push(list[idx++]);
  }

  return result;
}

module.exports = router;
