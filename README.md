# Web scrapping tools

# Configuration

Set uri to parse in .env file. See env.example:

# Installation

To use this application, you've two installation methods:

1. Node.js

If you have node 15 installed in your system can install the application by:

```
npm i
```

2. Container version.

You can create dockerized version by:

```
docker build -t myparser .
```

# Run application

If you want to run native version:

```
npm start
```

If you want to run dockerized version:
```
docker run -p 3000:3000 myparser
```

# Access data

To access data as json, access the next URI:

```
http://localhost:3000/parse
```

And you'll all data as JSON parsed from original table from original uri
configured on .env file.
