FROM node:15.11.0-alpine3.10

COPY . /app
WORKDIR /app
RUN npm i
EXPOSE 3000
CMD [ "npm", "start" ]
